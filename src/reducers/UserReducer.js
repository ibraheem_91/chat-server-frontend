import {Constants} from "../common/constants";
import {Map} from "immutable";
import {merge} from "ramda";

let initStateUser = Map({
    username: "",
});

export function UserReducer(state = initStateUser.toJS(), userAction) {

    switch (userAction.type) {
        case Constants.get('ADD_USER'):

            return merge(
                state, {
                    username: userAction.username
                });
            
            break;
    }
    return state;

}