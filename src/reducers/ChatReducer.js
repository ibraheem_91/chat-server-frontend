import {Constants} from "../common/constants";
import {Map} from "immutable";
import {merge} from "ramda";

let initStateChat = Map({
    currentMsg: "",
    username: "",
    allMsgs: [],
});


export function ChatReducer(state = initStateChat.toJS(), chatAction) {

    switch (chatAction.type) {
        case Constants.get('SEND_CHAT'):
            return merge(
                state,
                {
                    currentMsg: chatAction.currentMsg,
                    username: chatAction.username,
                }
            );

            break;

        case Constants.get('UPDATE_CHAT'):
            return merge(
                state,
                {
                    username: chatAction.username,
                    currentMsg: chatAction.currentMsg,
                    allMsgs: state.allMsgs.concat(chatAction.username + " : " + chatAction.currentMsg),
                });
            break;

    }

    return state;

}