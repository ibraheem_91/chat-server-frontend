import {combineReducers} from "redux";
import {ChatReducer} from "./ChatReducer";
import {RoomReducer} from "./RoomReducer";
import {UserReducer} from "./UserReducer";


const allReducers = combineReducers({
    chat: ChatReducer,
    room: RoomReducer,
    user: UserReducer
});

export default allReducers
