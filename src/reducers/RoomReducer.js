import {Constants} from "../common/constants";
import {Map} from "immutable";
import {merge} from "ramda";

let initStateRoom = Map({
    roomName: "room1",
    allRooms: []
});

export function RoomReducer(state = initStateRoom.toJS(), roomAction) {

    switch (roomAction.type) {
        case Constants.get('SHOW_ROOMS'):

            return merge(
                state, {
                    allRooms: roomAction.allRooms,
                    roomName: roomAction.roomName
                });

            break;
        case Constants.get('SWITCH_ROOM'):

            return merge(
                state, {
                    roomName: roomAction.roomName,
                });

            break;
    }
    return state;

}