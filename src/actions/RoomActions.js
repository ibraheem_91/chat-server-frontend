import {Constants} from "../common/constants";

export const showRoomsActionCreator = (allRooms, roomName) => {
    return {
        type: Constants.get('SHOW_ROOMS'),
        allRooms,
        roomName
    };
};


export const switchRoomActionCreator = (roomName) => {
    return {
        type: Constants.get('SWITCH_ROOM'),
        roomName
    };
};