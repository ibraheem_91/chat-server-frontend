import {Constants} from "../common/constants";

export const updateChatActionCreator = (username, currentMsg) => {
    return {
        type: Constants.get('UPDATE_CHAT'),
        username,
        currentMsg
    };
};

export const sendChatActionCreator = (username, currentMsg) => {
    return {
        type: Constants.get('SEND_CHAT'),
        username,
        currentMsg
    };
};
