import {Constants} from "../common/constants";

export const addNewUserActionCreator = (username) => {
    return {
        type: Constants.get('ADD_USER'),
        username
    };
};