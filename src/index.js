import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import {Provider} from "react-redux";
import {Route, Router, Switch} from "react-router";
import createHistory from "history/createBrowserHistory";
import {createStore} from "redux";
import allReducers from "./reducers";


const historyOfApp = createHistory({
    //forceRefresh: true
    hashType: 'hashbang'
});


/**
 *
 * @type {Store<S>}
 *
 * add config
 * add constants
 * add socket io class|chat class
 *
 */

const store = createStore(
    allReducers,
);


ReactDOM.render(
    <Provider store={store}>
        <Router history={historyOfApp}>
            <Switch >
                <Route exact path="/" component={App}/>
            </Switch>
        </Router>
    </Provider>

    , document.getElementById('root'));

registerServiceWorker();
