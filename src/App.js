import React, {Component} from "react";
import logo from "./logo.svg";
import "./App.css";
import AddNewUser from "./containers/AddNewUser";
import ChatMsgBox from "./containers/ChatMsgBox";
import ChatForm from "./containers/ChatForm";
import RoomListing from "./containers/RoomListing";

class App extends Component {

    render() {

        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Welcome to Chit-Chat app</h1>
                </header>
                <div className="App-intro">
                    <p>
                        Please enter your best name and let's proceed for chatting!
                    </p>

                    <AddNewUser />

                    <hr/>

                    <div>
                        <RoomListing/>
                    </div>

                    <hr/>
                    <div>
                        <h2>Chat Message Box</h2>
                        <ChatMsgBox/>
                        <ChatForm/>
                    </div>

                </div>

            </div>
        );
    }
}

export default App;
