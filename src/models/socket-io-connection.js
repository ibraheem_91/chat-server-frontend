import io from "socket.io-client";
import {Config} from "../config";

class SocketIOConnection {

    static getConnection() {

        return io(Config.get('SERVER_URL'));

    }

}

export {SocketIOConnection};