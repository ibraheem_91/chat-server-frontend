import React, {Component} from "react";
import {connect} from "react-redux";

class ChatMsgBox extends Component {

    constructor() {
        super();
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        return this.props.chat.allMsgs.map((msg, key) => {
            return (
                <div key={key} style={{marginBottom: "10px"}}>{msg}</div>
            );
        });

    }
}

function mapStateToProps(state) {
    return {
        chat: state.chat,
    };
}


export default  connect(mapStateToProps)(ChatMsgBox);
