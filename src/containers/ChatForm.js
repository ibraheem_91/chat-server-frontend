import React, {Component} from "react";
import {connect} from "react-redux";
import * as UserActionCreators from "../actions/UserActions";
import * as ChatActionCreators from "../actions/ChatActions";
import * as RoomActionCreators from "../actions/RoomActions";
import {bindActionCreators} from "redux";
import {Constants} from "../common/constants";
import {SocketIOConnection} from "../models/socket-io-connection";

class ChatForm extends Component {

    constructor() {
        super();

        this.onTypingMsg = this.onTypingMsg.bind(this);
        this.sendMsg = this.sendMsg.bind(this);
    }

    /**
     *
     * @param eventOfOnTypingMsg
     */
    onTypingMsg = (eventOfOnTypingMsg) => {
        //@todo: ramda
        let msg = eventOfOnTypingMsg.target.value;
        //dispatch an action to update the username state in the store
        let username = this.props.user.username;
        this.props.actions.chatActionCreator.sendChatActionCreator(username, msg);

    };


    sendMsg = () => {

        let socket = SocketIOConnection.getConnection();

        let self = this;
        socket.on('connect', () => {
            socket.emit(Constants.get('SEND_CHAT'), {
                msg: self.props.chat.currentMsg,
                username: self.props.user.username,
                room: self.props.room.roomName
            });

            socket.on(Constants.get('UPDATE_CHAT'), (username, msg, room) => {

                if (room === self.props.room.roomName) {

                    self.props.actions.chatActionCreator.updateChatActionCreator(username, msg);
                }

            });

        });


    };

    /**
     *
     * @returns {XML}
     */
    render() {
        return (
            <p>
                <input onChange={this.onTypingMsg} placeholder="Start typing...!" type="text" style={{fontSize: "18px", marginRight: "10px"}}/>

                <button onClick={this.sendMsg} style={{width: "200px", fontSize: "18px"}}>Send</button>

            </p>

        );

    }
}

function mapStateToProps(state) {
    return {
        chat: state.chat,
        user: state.user,
        room: state.room,
    };
}

/**
 *
 * @param dispatch
 * @returns {{actions: {userActionCreator: (A|B|M|N), chatActionCreator: (A|B|M|N)}}}
 */
function mapDispatchToProps(dispatch) {

    return {
        actions: {
            userActionCreator: bindActionCreators(UserActionCreators, dispatch),
            chatActionCreator: bindActionCreators(ChatActionCreators, dispatch),
            roomActionCreators: bindActionCreators(RoomActionCreators, dispatch),
        }
    };

}

export default connect(mapStateToProps, mapDispatchToProps)(ChatForm);
