import React, {Component} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as UserActionCreators from "../actions/UserActions";
import * as ChatActionCreators from "../actions/ChatActions";
import * as RoomActionCreators from "../actions/RoomActions";
import {Constants} from "../common/constants";
import {SocketIOConnection} from "../models/socket-io-connection";

class RoomListing extends Component {

    constructor() {
        super();
        this.switchRoom = this.switchRoom.bind(this);
    }

    switchRoom = (eventOfSwitchRoom) => {
        eventOfSwitchRoom.preventDefault();
        eventOfSwitchRoom.stopPropagation();


        let oldRoom = this.props.room.roomName;

        let newRoom = eventOfSwitchRoom.target.id;

        //switch room action fire change current room
        this.props.actions.roomActionCreators.switchRoomActionCreator(newRoom);


        let socket = SocketIOConnection.getConnection();

        let self = this;
        socket.on('connect', () => {
            //todo: use immutable.js here
            socket.emit(Constants.get('SWITCH_ROOM'), {
                oldRoom: oldRoom,
                newRoom: newRoom,
                username: self.props.user.username
            });

            socket.on(Constants.get('UPDATE_CHAT'), (username, msg, room) => {

                if (room === self.props.room.roomName) {
                    self.props.actions.chatActionCreator.updateChatActionCreator(username, msg);
                }

            });

            socket.on(Constants.get('UPDATE_ROOMS'), (allRooms, currentRoom) => {
                self.props.actions.roomActionCreators.showRoomsActionCreator(allRooms, currentRoom);
            });

        });


    };


    /**
     *
     * @returns {XML}
     */
    render() {
        let self = this;

        return this.props.room.allRooms.map((room, key) => {
            if (self.props.room.roomName === room) {
                return (
                    <p key={key}>
                        <button id={room} onClick={self.switchRoom} key={key} style={{color: "red", fontSize: "18px", width: "150px"}}> → {room} ←</button>
                    </p>
                );
            }
            return (
                <p key={key}>
                    <button id={room} onClick={self.switchRoom} style={{width: "150px"}} key={key}>{room}</button>
                </p>
            );
        });

    }
}

function mapStateToProps(state) {
    return {
        room: state.room,
        chat: state.chat,
        user: state.user,
    };
}


/**
 *
 * @param dispatch
 * @returns {{actions: {userActionCreator: (A|B|M|N), chatActionCreator: (A|B|M|N)}}}
 */
function mapDispatchToProps(dispatch) {

    return {
        actions: {
            userActionCreator: bindActionCreators(UserActionCreators, dispatch),
            chatActionCreator: bindActionCreators(ChatActionCreators, dispatch),
            roomActionCreators: bindActionCreators(RoomActionCreators, dispatch),
        }
    };

}


export default  connect(mapStateToProps, mapDispatchToProps)(RoomListing);
