import React, {Component} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as UserActionCreators from "../actions/UserActions";
import * as ChatActionCreators from "../actions/ChatActions";
import * as RoomActionCreators from "../actions/RoomActions";
import {Constants} from "../common/constants";
import {SocketIOConnection} from "../models/socket-io-connection";

class AddNewUser extends Component {

    constructor() {
        super();

        this.onUsernameChange = this.onUsernameChange.bind(this);
        this.addNewUser = this.addNewUser.bind(this);
    }

    /**
     *
     * @param eventOfUsernameChange
     */
    onUsernameChange = (eventOfUsernameChange) => {
        let username = eventOfUsernameChange.target.value;
        //dispatch an action to update the username state in the store
        this.props.actions.userActionCreator.addNewUserActionCreator(username);
    };

    /**
     *
     * @param eventOfAddNewUser
     */
    addNewUser = (eventOfAddNewUser) => {
        eventOfAddNewUser.preventDefault();
        eventOfAddNewUser.stopPropagation();

        let socket = SocketIOConnection.getConnection();

        let self = this;
        socket.on('connect', () => {
            //todo: use immutable.js here


            socket.emit(Constants.get('ADD_USER'), {username: self.props.user.username});

            socket.on(Constants.get('UPDATE_CHAT'), (username, msg, room) => {

                if (room === self.props.room.roomName) {
                    self.props.actions.chatActionCreator.updateChatActionCreator(username, msg);
                }

            });

            socket.on(Constants.get('UPDATE_ROOMS'), (allRooms, currentRoom) => {
                self.props.actions.roomActionCreators.showRoomsActionCreator(allRooms, currentRoom);
            });


        });

    };

    /**
     *
     * @returns {XML}
     */
    render() {
        return (
            <p>
                <input onChange={this.onUsernameChange} placeholder="Your best name!" type="text" id="nickname" style={{fontSize: "18px", marginRight: "10px"}}/>

                <button onClick={this.addNewUser} style={{width: "200px", fontSize: "18px"}}>Start chatting</button>

            </p>
        );
    }
}

/**
 *
 * @param state
 * @returns {{user: *}}
 */
function mapStateToProps(state) {
    return {
        user: state.user,
        chat: state.chat,
        room: state.room,
    };
}

/**
 *
 * @param dispatch
 * @returns {{actions: {userActionCreator: (A|B|M|N), chatActionCreator: (A|B|M|N)}}}
 */
function mapDispatchToProps(dispatch) {

    return {
        actions: {
            userActionCreator: bindActionCreators(UserActionCreators, dispatch),
            chatActionCreator: bindActionCreators(ChatActionCreators, dispatch),
            roomActionCreators: bindActionCreators(RoomActionCreators, dispatch),
        }
    };

}

export default connect(mapStateToProps, mapDispatchToProps)(AddNewUser);
